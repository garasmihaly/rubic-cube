﻿using DoozyUI;
using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace RubikCube.UI
{
    public class Login : MonoBehaviour
    {

        [SerializeField] UIButton closeLogin;
        [SerializeField] UIButton openButton;
        [SerializeField] UIButton showContinueButton;
        [SerializeField] TMPro.TextMeshProUGUI fbtext;
        [SerializeField] Image profileImage;

        private void Awake()
        {

            if (!FB.IsInitialized)
            {
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                FB.ActivateApp();
            }

        }


        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }

        private void ShowMainMenu()
        {
            closeLogin.ExecuteClick();
            if (Providers.AppDatas.AppSettings.LastSteps.Count != 0)
            {
                showContinueButton.ExecuteClick();
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }

        public void OnLogin()
        {
            var perms = new List<string>() { "public_profile", "email" };
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }

        public void OnLogout()
        {
            if (FB.IsLoggedIn)
            {
                FB.LogOut();
                StartCoroutine("CheckForSuccussfulLogout");
            }
        }

        IEnumerator CheckForSuccussfulLogout()
        {
            if (FB.IsLoggedIn)
            {
                yield return new WaitForSeconds(0.1f);
                StartCoroutine("CheckForSuccussfulLogout");
            }
            else
            {
                EnableFacebookLoginButton();
            }
        }

        private void EnableFacebookLoginButton()
        {
            openButton.ExecuteClick();
        }

        private void AuthCallback(ILoginResult result)
        {
            if (FB.IsLoggedIn)
            {
                ShowMainMenu();
                FB.API("/me?fields=name", HttpMethod.GET, DisplayName);
                FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, GetPicture);
                var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            }
        }

        private void DisplayName(IResult result)
        {
            if (result.Error != null)
            {
                fbtext.text = result.Error;
            }
            else
            {
                fbtext.text = "Hi there: <b>" + result.ResultDictionary["name"] + "<b>";
            }
        }

        private void GetPicture(IGraphResult result)
        {
            if (string.IsNullOrEmpty(result.Error))
            {
                Debug.Log("received texture with resolution " + result.Texture.width + "x" + result.Texture.height);
                profileImage.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2(0, 0));
            }
        }
    }
}
