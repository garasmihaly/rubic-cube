﻿using DoozyUI;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace RubikCube.UI
{
    public class MainMenu : MonoBehaviour
    {

        /// <summary>
        /// Doozy Ui button which can open the main menu 
        /// </summary>
        [SerializeField]
        UIButton openMenu;

        /// <summary>
        /// Doozy Ui button wich can open the timer challange view
        /// </summary>
        [SerializeField]
        UIButton openTimerChallange;

        /// <summary>
        /// Doozy Ui button wich can show a popup message
        /// </summary>
        [SerializeField]
        UIButton showPopupMessageButton;

        /// <summary>
        /// Clock's text
        /// </summary>
        [SerializeField]
        TMPro.TextMeshProUGUI Timer;

        /// <summary>
        /// Popup message's text
        /// </summary>
        [SerializeField]
        TMPro.TextMeshProUGUI message;

        /// <summary>
        /// flag when the shared file is ready
        /// </summary>
        private bool sharedfile_is_ready = false;

        /// <summary>
        /// The photo's path
        /// </summary>
        string mediaPath;

        #region Monobehaviour methods
        private void LateUpdate()
        {
            if (sharedfile_is_ready)
            {
                NativeShare();
            }
        }
        #endregion

        #region User interactions

        /// <summary>
        /// New game button's press handle
        /// </summary>
        public void OnNewGame()
        {
            openMenu.ExecuteClick();
            RubicWorld.Instance.ResetCube();
            RubicWorld.Instance.rubicCube.OnShuffle(false);
        }

        /// <summary>
        /// Back to main menu button's press handle
        /// </summary>
        public void OnBackToMain()
        {
            RubicWorld.Instance.rubicCube.ResetTimer();
        }

        /// <summary>
        /// New Time cahllenge button's press handle
        /// </summary>
        public void OnNewTimeChallange()
        {
            openMenu.ExecuteClick();
            openTimerChallange.ExecuteClick();
            RubicWorld.Instance.ResetCube();
            RubicWorld.Instance.rubicCube.OnShuffle(true);
        }

        /// <summary>
        /// Solve the rubic cube button's press handle
        /// </summary>
        public void OnSolve()
        {
            RubicWorld.Instance.rubicCube.OnSolve();
        }

        /// <summary>
        /// Shuffle cube button's handle
        /// </summary>
        public void OnSuffle()
        {
            RubicWorld.Instance.rubicCube.OnShuffle(false);
        }

        /// <summary>
        /// Reset the cube button's handle
        /// </summary>
        public void OnReset()
        {
            RubicWorld.Instance.ResetCube();
        }

        /// <summary>
        /// Share button's press handle
        /// </summary>
        public void OnShare()
        {

            mediaPath = Path.Combine(Application.persistentDataPath, System.Guid.NewGuid().ToString() + "share.png");

            ScreenCapture.CaptureScreenshot(mediaPath);
            //StartCoroutine(WaitForFile(mediaPath));
            NativeShare();
        }

        #endregion

        /// <summary>
        /// this method can sow a popup message on screen
        /// </summary>
        /// <param name="message">The message of popup</param>
        public void ShowPopupMessage(string message)
        {
            this.message.text = message;
            showPopupMessageButton.ExecuteClick();
        }

        /// <summary>
        /// This method can set the clock
        /// </summary>
        /// <param name="sec"></param>
        public void SetTimer(int sec)
        {
            this.Timer.text = sec.ToString();
        }

        /// <summary>
        /// this coroutine waiting while the shared file dosnt ready 
        /// </summary>
        /// <param name="path">The file path</param>
        /// <returns></returns>
        IEnumerator WaitForFile(string path)
        {
            while (!File.Exists(path))
            {
                yield return null;
            }
            sharedfile_is_ready = true;
        }

        /// <summary>
        /// Start Native sharing
        /// </summary>
        void NativeShare()
        {
            sharedfile_is_ready = false;
            NativeShare ns = new NativeShare();
            ns.SetSubject("Shared from rubic cube 3D");
            ns.SetText("Shared from rubic cube 3D - description");
            ns.AddFile(mediaPath);
            ns.Share();
        }
    }
}
