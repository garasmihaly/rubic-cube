﻿using RubikCube.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RubikCube
{
    public class RubicWorld : MonoBehaviour
    {

        [SerializeField]
        RubikCube cubePrefab;

        /// <summary>
        /// Main Menu's script for fast reach
        /// </summary>
        public MainMenu mainMenu;

        /// <summary>
        /// The cube's  script
        /// </summary>
        public RubikCube rubicCube
        {
            get { return GetComponentInChildren<RubikCube>(); }
        }

        /// <summary>
        /// private instance
        /// </summary>
        static RubicWorld instance;

        /// <summary>
        /// Singleton's instance
        /// </summary>
        public static RubicWorld Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.Find("Rubic World").gameObject.GetComponent<RubicWorld>();
                }
                return instance;
            }
        }

        /// <summary>
        /// Reset the cube
        /// </summary>
        public void ResetCube()
        {
            Providers.AppDatas.AppSettings.LastSteps = new List<KeyValuePair<string, int>>();
            GameObject.DestroyImmediate(RubicWorld.Instance.rubicCube.gameObject);
            RubikCube rc = Instantiate<RubikCube>(cubePrefab);
            Providers.AppDatas.AppSettings.LastSteps = new List<KeyValuePair<string, int>>();
            rc.transform.SetParent(RubicWorld.Instance.transform);
        }


    }
}
