﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace RubikCube
{

    public enum RotationAxis
    {
        X, Y, Z
    }

    public enum AxisPositionType
    {
        Side,
        Center
    }

    public enum BlockPositionType
    {
        Center,
        Corner,
        Edge
    }

    public enum PlaneNumber
    {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX
    }

    public enum RotationDirection
    {
        Forward,
        Backward
    }
}


