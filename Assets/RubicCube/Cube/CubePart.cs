﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures.TransformGestures;
using RubikCube.Core.Base;
using System;
using UnityEngine.EventSystems;
using TouchScript.Gestures;
using RubikCube.Core;
using System.Linq;

namespace RubikCube
{
    public class CubePart : TouchComponent, ICubePart
    {
        public BlockPositionType ptype;

        private Vector3 First = Vector3.zero;
        private Vector3 AFirst = Vector3.zero;
        private Vector3 Second = Vector3.zero;
        private RotationAxis axis;
        private RotationDirection direction;
        private ColoredPlane cp;
        private CubeAxis _axis = null;
        private bool handled = false;
        private float startrotation = 0;
        
        private Collider collider;
        public Collider Collider
        {
            get
            {
                if (this.collider == null) this.collider = GetComponentInChildren<Collider>();
                return collider;
            }
        }

        #region Monobehaviour methods
        protected override void Awake()
        {
            base.Awake();
            if (this.name.Contains("Center")) ptype = BlockPositionType.Center;
            else if (this.name.Contains("Corner")) ptype = BlockPositionType.Corner;
            else if (this.name.Contains("Edge")) ptype = BlockPositionType.Edge;
            EventBus.Instance.register<ICubePart>(this.gameObject);
        }

        protected override void OnDestroy()
        {
            EventBus.Instance.unregister(this.gameObject);
            base.OnDestroy();
        }
        protected override void Update()
        {
            base.Update();
            Debug.DrawLine(AFirst, Second);
        }
        #endregion

        #region Getures handling
        protected override void GReleaseGesture(ReleaseGesture sender, EventArgs e)
        {
            Debug.Log("Release");
            base.GReleaseGesture(sender, e);
            direction = GetRotateDirection() ? RotationDirection.Forward : RotationDirection.Backward;
            ExecuteEvents.Execute<ICubeAxis>(_axis.gameObject, null, (x, y) => x.rotate(direction,false));
            Debug.Log("First=zero");
            First = Vector3.zero;
            handled = false;
            _axis = null;
        }

        protected override void GPanGesture(ScreenTransformGesture sender, EventArgs e)
        {
            if (First == Vector3.zero)
            {
                RaycastHit hit;
                RaycastFromScreen(out First, out hit, sender.ScreenPosition, true);
                if (First == Vector3.zero) return;

            }
            else if (!handled)
            {
                RaycastHit hit;
                RaycastFromScreen(out Second, out hit, sender.ScreenPosition, false);
                if (Second == Vector3.zero) return;
                //if (this.cp == null && hit.transform != null) cp = hit.transform.GetComponent<ColoredPlane>();
                Vector3 delta = (First - Second);
                float max = Mathf.Max(Mathf.Max(Mathf.Abs(delta.x), Mathf.Abs(delta.y)), Mathf.Abs(delta.z));

                EventBus.Instance.post<ICubeAxis>((x, d) => x.Release(false));
                Debug.Log(delta);

                if (Mathf.Abs(delta.x) == max) SelectAxis(Second, RotationAxis.X);
                else if (Mathf.Abs(delta.y) == max) SelectAxis(Second, RotationAxis.Y);
                else if (Mathf.Abs(delta.z) == max) SelectAxis(Second, RotationAxis.Z);
                if (_axis != null)
                {
                    startrotation = GetRotationBasedOnAxis();
                }
                Debug.Log("selectedaxis: " + Enum.GetName(typeof(RotationAxis), axis));

            }
            else
            {
                RaycastHit hit;
                RaycastFromScreen(out Second, out hit, sender.ScreenPosition, false);
                if (Second == Vector3.zero) return;
                float delta = Vector3.Angle(First, Second);
                //direction = DetectClockwise(First, Second) ? RotationDirection.Backward : RotationDirection.Forward;
                if (_axis != null) ExecuteEvents.Execute<ICubeAxis>(_axis.gameObject, null, (x, y) => x.rotate(Second));
                First = Second;
                Debug.Log("First=Second");
            }
            base.GPanGesture(sender, e);
        }

        public void blocktouch(bool block)
        {
            if (block) BlockTouch();
            else AllowTouch();
        }
        #endregion

        private bool GetRotateDirection()
        {
            float fromY = GetRotationBasedOnAxis();
            float toY = startrotation;
            float clockWise = 0f;
            float counterClockWise = 0f;

            if (fromY <= toY)
            {
                clockWise = toY - fromY;
                counterClockWise = fromY + (360 - toY);
            }
            else
            {
                clockWise = (360 - fromY) + toY;
                counterClockWise = fromY - toY;
            }
            return (clockWise <= counterClockWise);
        }

        public float GetRotationBasedOnAxis()
        {

            switch (this._axis.axis)
            {
                case RotationAxis.X:
                    return this._axis.transform.rotation.eulerAngles.x;
                case RotationAxis.Y:
                    return this._axis.transform.rotation.eulerAngles.y;
                case RotationAxis.Z:
                    return this._axis.transform.rotation.eulerAngles.z;
            }
            return 0;
        }

        private void SelectAxis(Vector3 Second, RotationAxis selected)
        {
            axis = selected;
            handled = true;
            Debug.Log("First=Second");
            First = Second;
            searchAxis();
        }

        private void RaycastFromScreen(out Vector3 worldposition, out RaycastHit hit, Vector3 screenposition, bool first)
        {
            Ray ray = Camera.main.ScreenPointToRay(screenposition);
            RaycastHit[] hits = Physics.RaycastAll(ray, float.MaxValue);
            hits = hits.OrderBy(wo => wo.distance).ToArray();
            if (first)
            {
                Debug.Log("First raycast");
                if (hits.Count() > 1)
                {
                    Debug.Log("hits 0 name:" + hits[0].transform.name);
                    Debug.Log("hits 1 name:" + hits[1].transform.name);
                    Debug.Log("hits 2 name:" + hits[2].transform.name);
                    Debug.Log("hits 3 name:" + hits[3].transform.name);

                    cp = hits[1].transform.GetComponent<ColoredPlane>();
                }
                else
                {
                    Debug.Log("why");
                }
                AFirst = hits[0].point;
            }
            else
            {
                Debug.Log("second raycast");
            }

            if (hits.Count() == 0)
            {
                hit = new RaycastHit();
                worldposition = Vector3.zero;
            }

            else
            {
                hit = hits[0];
                worldposition = hits[0].point;
            }


        }

        private CubeAxis searchAxis()
        {
            if (_axis == null)
            {
                List<CubeAxis> list = new List<CubeAxis>();
                foreach (CubeAxis ca in GameObject.FindObjectsOfType<CubeAxis>())
                {
                    Collider c1 = ca.Collider;
                    Collider c2 = Collider;

                    if (this.ptype == BlockPositionType.Center && c1.bounds.Intersects(c2.bounds) && ca.axis != axis && ca.ptype != AxisPositionType.Side)
                    {
                        _axis = ca;
                        list.Add(ca);
                    }
                    else if (this.ptype == BlockPositionType.Edge && c1.bounds.Intersects(c2.bounds))
                    {
                        _axis = ca;
                        list.Add(ca);
                    }
                    else if (this.ptype == BlockPositionType.Corner && c1.bounds.Intersects(c2.bounds))
                    {
                        _axis = ca;
                        list.Add(ca);
                    }
                }
                if (list.Count > 1)
                {
                    if (cp != null)
                    {
                        Debug.Log("cp!=null");
                        int index = cp.way.IndexOf(axis);
                        if (index != -1) _axis = cp.axis[index];
                        Debug.Log("wayindex" + index);
                    }
                    else
                    {
                        Debug.Log("cp is null");
                    }

                }

                CubeAxis caxis = _axis.GetComponent<CubeAxis>();
                caxis.ResetRotationHelper(First);




            }
            return _axis;

        }

    }

    public interface ICubePart : IEventSystemHandler
    {
        void blocktouch(bool block);
    }
}
