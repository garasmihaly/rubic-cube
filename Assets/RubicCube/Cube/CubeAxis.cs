﻿using RubikCube.Core;
using RubikCube.Core.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RubikCube
{
    public class CubeAxis : MonoBehaviour, ICubeAxis
    {
        const float speed = 25;

        public RotationAxis axis;
        public AxisPositionType ptype;
        public GameObject RotationHelper;

        private float degree;
        private bool release = true;

        private Collider collider;
        public Collider Collider
        {
            get
            {
                if (this.collider == null) this.collider = GetComponentInChildren<Collider>();
                return collider;
            }
        }

        #region Monobehaviour methods

        private void Awake()
        {
            EventBus.Instance.register<ICubeAxis>(this.gameObject);
        }

        private void OnDestroy()
        {
            EventBus.Instance.unregister(this.gameObject);
        }

        void Update()
        {
            if (release)
            {
                Quaternion newrotation = Quaternion.Euler(axis == RotationAxis.X ? degree : 0,
                                 axis == RotationAxis.Y ? degree : 0,
                                 axis == RotationAxis.Z ? degree : 0);
                transform.rotation = Quaternion.Slerp(transform.rotation, newrotation, Time.deltaTime * speed);
            }
        }
        #endregion

        private void SetAxis(float degree)
        {
            Release(true);
            this.degree += degree;
            this.degree = this.degree - this.degree % 90;
            if (Mathf.Abs(this.degree) == 360) this.degree = 0;
        }

        private Vector3 RotationBasedOnAxis(float degree)
        {
            switch (this.axis)
            {
                case RotationAxis.X:
                    return Vector3.right * degree;
                case RotationAxis.Y:
                    return Vector3.up * degree;
                case RotationAxis.Z:
                    return Vector3.forward * degree;
            }
            return Vector3.zero;
        }

        private void SetPartsParent()
        {
            IEnumerable<CubePart> blocks = RubicWorld.Instance.rubicCube.GetComponentsInChildren<CubePart>();
            List<CubePart> ondisk = new List<CubePart>();
            foreach (CubePart cp in blocks)
            {

                if (Collider.bounds.Intersects(cp.Collider.bounds))
                {
                    ondisk.Add(cp);
                    cp.transform.SetParent(this.transform);
                }
            }
        }

        private void ResetSetPartsParent()
        {
            foreach (CubePart cp in RubicWorld.Instance.rubicCube.GetComponentsInChildren<CubePart>())
            {
                cp.transform.SetParent(GetComponentInParent<RubikCube>().transform);
            }
        }

        public void ResetRotationHelper(Vector3 firstposition)
        {
            ResetSetPartsParent();
            if (RotationHelper == null)
            {
                RotationHelper = new GameObject();
                RotationHelper.name = this.name + "_RHelper";
                RotationHelper.transform.position = transform.position;
            }
            RotationHelper.transform.SetParent(RubicWorld.Instance.rubicCube.transform);
            LookAt(firstposition);
            this.transform.SetParent(RotationHelper.transform);
            SetPartsParent();
        }

        private void LookAt(Vector3 position)
        {
            Debug.Log("LookatPosition: " + position + "Axis: " + axis);
            RotationHelper.transform.LookAt(position);
            Transform rotated = this.axis == RotationAxis.Z ? this.transform : RotationHelper.transform;
            rotated.rotation = Quaternion.Euler(RotationBasedOnAxis(rotated.rotation.eulerAngles));
        }

        private Vector3 RotationBasedOnAxis(Vector3 rotation)
        {
            Vector3 correctedrotation = Vector3.zero;
            switch (this.axis)
            {
                case RotationAxis.X:
                    correctedrotation = Vector3.Scale(rotation, Vector3.right);
                    break;
                case RotationAxis.Y:
                    correctedrotation = Vector3.Scale(rotation, Vector3.up);
                    break;
                case RotationAxis.Z:
                    correctedrotation = Vector3.Scale(rotation, Vector3.forward);
                    //Debug.Log("LookatRotation" + rotation);
                    break;
            }
            return correctedrotation;
        }

        #region Event's methods
        public void Release(bool release)
        {
            this.release = release;
            if (release) this.transform.SetParent(RubicWorld.Instance.rubicCube.transform);
        }

        public void rotate(Vector3 wordposition)
        {
            LookAt(wordposition);
        }

        public void rotate(RotationDirection direction,bool solve)
        {
            SetPartsParent();
            float angle = direction == RotationDirection.Backward ? 90 : -90;
            SetAxis(angle);
            RubicWorld.Instance.rubicCube.NewStep(this, direction,solve);
        }

        public void rotatefast(RotationDirection direction)
        {
            SetPartsParent();
            SetAxis(direction == RotationDirection.Backward ? 90 : -90);
            Quaternion newrotation = Quaternion.Euler(axis == RotationAxis.X ? degree : 0,
                                 axis == RotationAxis.Y ? degree : 0,
                                 axis == RotationAxis.Z ? degree : 0);
            this.transform.rotation = newrotation;
        }
        #endregion
    }


    public interface ICubeAxis : IEventSystemHandler
    {
        void Release(bool release);
        void rotate(Vector3 wordposition);
        void rotate(RotationDirection direction,bool solve);
        void rotatefast(RotationDirection direction);

    }
}