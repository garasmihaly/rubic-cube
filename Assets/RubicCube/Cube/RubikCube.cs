﻿using RubikCube.Core;
using RubikCube.Core.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;


namespace RubikCube
{
    public class RubikCube : MonoBehaviour
    {
        const int ShufleSteps = 20;
        const int TimerSeconds = 60;

        private bool shuffle = false;
        private bool solve = false;
        private bool timerchallenge = false;
        private List<KeyValuePair<string, int>> steps = new List<KeyValuePair<string, int>>();
        private System.Random r;
        private int ssteps = ShufleSteps;
        private int timer = TimerSeconds;

        #region Monobehaviour methods
        private void Awake()
        {
            r = new System.Random();
            steps = Providers.AppDatas.AppSettings.LastSteps;
        }

        private void Start()
        {
            if (steps.Count != 0) StartCoroutine(ResetLastState());
        }

        public void Update()
        {
            if (shuffle)
            {
                StartCoroutine(ShuffleRandom());
            }

            if (solve)
            {
                StartCoroutine(Solve(true));
            }
        }
        #endregion

        private void IncreaseTime()
        {
            timer--;
            RubicWorld.Instance.mainMenu.SetTimer(timer);
            if (timer == 0)
            {
                RubicWorld.Instance.mainMenu.ShowPopupMessage("YOU LOST :(");
                CancelInvoke();
            }
        }
        
        public void OnShuffle(bool timerchallange)
        {
            EventBus.Instance.post<ICubePart>((e, d) => e.blocktouch(true));
            shuffle = true;
            solve = false;
            this.timerchallenge = timerchallange;
            if (timerchallange)
            {
                timer = TimerSeconds;
                RubicWorld.Instance.mainMenu.SetTimer(timer);
            }
        }

        public void OnSolve()
        {
            if (steps.Count == 0) return;
            EventBus.Instance.post<ICubePart>((e, d) => e.blocktouch(true));
            shuffle = false;
            ssteps = ShufleSteps;
            solve = true;
        }

        public void ResetTimer()
        {
            CancelInvoke();
        }

        private IEnumerator ResetLastState()
        {
            EventBus.Instance.post<ICubeAxis>((e, d) => e.Release(false));
            foreach (KeyValuePair<string, int> step in steps)
            {
                yield return null;
                CubeAxis ca;
                RotationDirection rd;
                GetStepDatas(step, out ca, out rd);
                ExecuteEvents.Execute<ICubeAxis>(ca.gameObject, null, (x, y) => x.rotatefast(rd));
            }
            EventBus.Instance.post<ICubeAxis>((e, d) => e.Release(true));
        }


        IEnumerator Solve(bool animate)
        {
            solve = false;
            yield return new WaitForSeconds(0.25f);

            KeyValuePair<string, int> step = steps[steps.Count - 1];
            steps.RemoveAt(steps.Count - 1);
            CubeAxis ca;
            RotationDirection rd;
            GetStepDatas(step, out ca, out rd);
            if (animate) ExecuteEvents.Execute<ICubeAxis>(ca.gameObject, null, (x, y) => x.rotate(rd,true));
            else ExecuteEvents.Execute<ICubeAxis>(ca.gameObject, null, (x, y) => x.rotatefast(rd));
            if (steps.Count == 0)
            {
                solve = false;
                Providers.AppDatas.AppSettings.LastSteps = steps;
                EventBus.Instance.post<ICubePart>((e, d) => e.blocktouch(false));
            }
            else
            {
                solve = true;
            }

        }

        private void GetStepDatas(KeyValuePair<string, int> step, out CubeAxis ca, out RotationDirection rd)
        {
            ca = GetComponentsInChildren<CubeAxis>().Where(x => x.name == step.Key).FirstOrDefault();
            rd = step.Value == 0 ? RotationDirection.Backward : RotationDirection.Forward;
        }

        IEnumerator ShuffleRandom()
        {
            shuffle = false;
            yield return new WaitForSeconds(0.25f);

            ssteps -= 1;
            if (ssteps == 0)
            {
                shuffle = false;
                ssteps = ShufleSteps;
                EventBus.Instance.post<ICubePart>((e, d) => e.blocktouch(false));
                if(timerchallenge) InvokeRepeating("IncreaseTime", 0f, 1.0f);
            }
            else
            {
                shuffle = true;
                int next = r.Next(0, 8);
                CubeAxis ca = GetComponentsInChildren<CubeAxis>()[next];
                RotationDirection rd = (RotationDirection)r.Next(0, 1);
                Debug.Log(Enum.GetName(typeof(RotationDirection), rd));
                ExecuteEvents.Execute<ICubeAxis>(ca.gameObject, null, (x, y) => x.rotate(rd,false));
            }


        }

        public void NewStep(CubeAxis ca, RotationDirection rd,bool solve)
        {
            Debug.Log("Saved direction:" + Enum.GetName(typeof(RotationDirection), rd));
            if (!solve) steps.Add(new KeyValuePair<string, int>(ca.name, (int)rd));
            Providers.AppDatas.AppSettings.LastSteps = steps;
        }
    }
}


