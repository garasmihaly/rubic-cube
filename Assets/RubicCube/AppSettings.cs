using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

namespace RubikCube.Providers.AppDatas
{
    public sealed class AppSettings
    {

        enum appkeys
        {
            LAST_STATE,          
        }

        public static List<KeyValuePair<string,int>> LastSteps
        {
            get
            {
                string firststate = JsonConvert.SerializeObject(new List<KeyValuePair<string, int>>() { });
                string steps = PlayerPrefs.GetString("appdata" + appkeys.LAST_STATE.ToString(), firststate);
                return steps == "" ? new List<KeyValuePair<string, int>>() : JsonConvert.DeserializeObject<List<KeyValuePair<string, int>>>(steps);
            }
            set
            {
                PlayerPrefs.SetString("appdata" + appkeys.LAST_STATE.ToString(), JsonConvert.SerializeObject(value));
            }
        }

    }
}
