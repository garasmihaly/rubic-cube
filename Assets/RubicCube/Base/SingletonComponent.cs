using System.Linq;

namespace RubikCube.Core.Base
{
    public abstract class SingletonComponent : BaseComponent
    {
        protected override void Awake() {
            base.Awake();
			//clearComponents();           
        }

        private void clearComponents()
        {
            var comps = GetComponents<SingletonComponent>().Where(c => c != this);
            foreach (var comp in comps)
                comp.Dispose();
        }
    }
}

