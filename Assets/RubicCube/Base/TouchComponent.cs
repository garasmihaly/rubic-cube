using TouchScript.Gestures;
using System;
using System.Linq;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
//using UI;

namespace RubikCube.Core.Base
{
    public abstract class TouchComponent : BaseComponent
    {
        #region private fields

        private TapGesture tapGesture;
        private ScreenTransformGesture transformGesture;
        private ReleaseGesture releaseGesture;
        private LongPressGesture longpressGesture;
        private bool block = false;

        #endregion

        #region virtual methods

        protected virtual void GTapGesture(TapGesture sender, EventArgs e) { }
        protected virtual void GPanGesture(ScreenTransformGesture sender, EventArgs e) { }
        protected virtual void GReleaseGesture(ReleaseGesture sender, EventArgs e) { }
        protected virtual void GLongPressGesture(LongPressGesture sender, EventArgs e) { }
        protected virtual void GScaleGesture(ScreenTransformGesture sender, EventArgs e) { }

        #endregion

        #region Monobehaviour methods

        protected override void OnDestroy()
        {
            try
            {
                disconnectGestures();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.ToString());
            }
            finally
            {
                //EventBus.Instance.unregister(gameObject);
                base.OnDestroy();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            disconnectGestures();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            initGestures();
        }

        #endregion

        #region Gestures

        private void disconnectGestures()
        {
            if (tapGesture != null)
            {
                tapGesture.Tapped -= tappedHandler;
            }

            if (transformGesture != null)
            {
                transformGesture.Transformed -= transformHandler;
            }

            if (releaseGesture != null)
            {
                releaseGesture.Released -= releaseHandler;
            }
        }

        private void initGestures()
        {
            tapGesture = transform.GetComponent<TapGesture>();
            if (tapGesture != null)
            {
                tapGesture.Tapped += tappedHandler;
            }

            longpressGesture = transform.GetComponent<LongPressGesture>();
            if (longpressGesture != null)
            {
                longpressGesture.LongPressed += longpressHandler;
            }

            transformGesture = transform.GetComponent<ScreenTransformGesture>();
            if (transformGesture != null)
            {
               // transformGesture.SendStateChangeMessages = true;
                transformGesture.Transformed += transformHandler;
            }
            
            releaseGesture = transform.GetComponent<ReleaseGesture>();
            if (releaseGesture != null)
            {
                releaseGesture.Released += releaseHandler;

            }
        }

        private void transformHandler(object sender, EventArgs e)
        {
            if (block) return;
            if ((sender as ScreenTransformGesture).ActivePointers.Count == 1)
            {
                GPanGesture(sender as ScreenTransformGesture, e);
            }
            else if ((sender as ScreenTransformGesture).ActivePointers.Count == 2)
            {
                GScaleGesture(sender as ScreenTransformGesture, e);
            }
        }

        private void longpressHandler(object sender, EventArgs e)
        {
            if (block) return;
            GLongPressGesture(sender as LongPressGesture, e);
        }

        private void tappedHandler(object sender, EventArgs e)
        {
            if (block) return;
            GTapGesture(sender as TapGesture, e);
        }



        private void releaseHandler(object sender, EventArgs e)
        {
            GReleaseGesture(sender as ReleaseGesture, e);
        }

        public void BlockTouch()
        {
            block = true;
        }

        public void AllowTouch()
        {
            block = false;
        }

        #endregion

    }
}

