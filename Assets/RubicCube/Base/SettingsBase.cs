﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace RubikCube.Providers.AppDatas
{
	public static class SettingsBase {

		public static string Serialize(Dictionary<string,int> value)
		{
			//var json=LitJson.JsonMapper.ToJson (value);
			var json = JsonConvert.SerializeObject(value);
			return  json ;
		}    

        public static string Serialize(List<string> value)
		{
			return  string.Join(";", value.ToArray()) ;
		}

		public static Dictionary<string,int> DeSerialize(string value)
		{
			var json = JsonConvert.DeserializeObject<Dictionary<string,int>> (value);
			return  json ;	
		}
      

        public static List<string> DeSerializeList(string value)
		{
			return  value.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries).ToList();
		}


	}
}
