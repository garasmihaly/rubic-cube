using System;
using UnityEngine;

namespace RubikCube.Core.Base
{
    public abstract class BaseComponent : MonoBehaviour, IDisposable
    {
        protected virtual void Awake() { }

        protected virtual void OnEnable() { }

        protected virtual void OnDisable() { }

        protected virtual void Update() { }

        protected virtual void LateUpdate() { }

		protected virtual void Start() { }

        protected virtual void OnDestroy() { }
        protected virtual void OnCollisionEnter(Collision col) { }
        protected virtual void OnTriggerEnter() { }

        public virtual void Dispose() {
            DestroyImmediate(this);
        }
    }
}

