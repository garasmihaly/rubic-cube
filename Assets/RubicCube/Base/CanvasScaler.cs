﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace RubikCube.UI
{
    public class CanvasScaler : UIBehaviour
    {
        private UnityEngine.UI.CanvasScaler scaler {
			get { return GetComponent<UnityEngine.UI.CanvasScaler>() ?? GetComponentInParent<UnityEngine.UI.CanvasScaler>(); }
        }

		public float ScaleFactor
		{
			get
            {
                return scaler.scaleFactor;
            }
		}
        protected override void Start() {
            base.Start();
            scaler.scaleFactor = Screen.dpi / 150;
        }
        
        public Vector2 scale(Vector2 value) {
            return Vector2.Scale(value, new Vector2(1 / scaler.scaleFactor, 1 / scaler.scaleFactor));
        }

        public float scale(float value) {
            return value / scaler.scaleFactor;
        }

        public float rescale(float value)
        {
            var factor = scaler.scaleFactor;
            var result = new Vector2(value, 0);
            result.Scale(new Vector2(factor, factor));
            return result.x;
        }

        public Vector2 rescale(Vector2 value) {
            return Vector2.Scale(value, new Vector2(scaler.scaleFactor, scaler.scaleFactor));
		}
    }
}

