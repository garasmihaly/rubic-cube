using TouchScript.Gestures;
using RubikCube.Core.Base;
using System;
using UnityEngine;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Layers;
using RubikCube.Extensions;

namespace RubikCube.Components
{ 
    public class CameraHandlerComponent : TouchComponent
    {
        #region private fields
        private Vector3 LastMousePosition = Vector3.zero;
        private Vector3 center = new Vector3(0, 0, 0);
        private Vector3 scaleCenter;
        private bool scaleActive;
        private float degX, degY;
        #endregion

        /// <summary>
        /// The camera
        /// </summary>
        [SerializeField] Camera currentCam;
        
        #region navigation's properties 
        public float rotateSpeed = 1f;
        public float scaleSpeed = 20f;
        #endregion

        #region monobehaviour methods
        protected override void Awake()
        {
            base.Awake();
            this.gameObject.AddComponent<FullscreenLayer>();
        }
        #endregion

        #region Gestures

        /// <summary>
        /// Handle Pan gesture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void GPanGesture(ScreenTransformGesture sender, EventArgs e)
        {
            base.GPanGesture(sender, e);
            if (LastMousePosition != Vector3.zero)
                Rotate();
            LastMousePosition = Input.mousePosition;
        }

        /// <summary>
        /// Handle scale gesture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void GScaleGesture(ScreenTransformGesture sender, EventArgs e)
        {
            base.GScaleGesture(sender, e);
            Zoom(sender.DeltaScale, false);
        }

        /// <summary>
        /// Handle release gesture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void GReleaseGesture(ReleaseGesture sender, EventArgs e)
        {
            base.GReleaseGesture(sender, e);
            LastMousePosition = Vector3.zero;
        }
        #endregion

        #region Navigations
        /// <summary>
        /// Zoom handling
        /// </summary>
        /// <param name="localdeltascale">Delta scale</param>
        /// <param name="wheel">if use mouse it will be true</param>
        public void Zoom(float localdeltascale, bool wheel)
        {
            var deltaScale = localdeltascale - 1;

            if (currentCam != null)
            {
                if (currentCam.transform.position.y + (currentCam.transform.forward.y * deltaScale * scaleSpeed) < 0)
                    return;
                currentCam.transform.position = currentCam.transform.position + (currentCam.transform.forward * deltaScale * scaleSpeed);
            }
        }

        /// <summary>
        /// Rotate camera around the cube
        /// </summary>
        void Rotate()
        {
            center = Vector3.zero;
            var distance = Vector3.Distance(currentCam.transform.position, center);
            var delta = Input.mousePosition - LastMousePosition;
            if (LastMousePosition == Vector3.zero) delta = Vector3.zero;
            var deltaX = delta.x * rotateSpeed;
            var deltaY = delta.y * rotateSpeed;
            degX += deltaX;
            degY += -deltaY;
            degY = degY.ClampAngle(-60f, 180);
            var rotation = Quaternion.Euler(degY, degX, 0);
            var position = rotation * (Vector3.back * distance) + center;
            currentCam.transform.rotation = rotation;
            currentCam.transform.position = position;
        }
        #endregion
    }
}

